<?php 
	require"../controllers/connection.php";

	// retrieve products for product_list.php

	$get_products = "SELECT * FROM products t1 JOIN sub_categories t2 ON t1.sub_category_id = t2.sub_category_id JOIN categories t3 ON t1.category_id = t3.category_id";

	$run_products = mysqli_query($conn, $get_products);


	// retrieve categories table for product_form.php
	$get_categories = "SELECT * FROM categories";
	$run_categories = mysqli_query($conn, $get_categories);

	//retrieve sub_categories for product_form.php
	$get_sub_categories = "SELECT * FROM sub_categories";
	$run_sub_categories = mysqli_query($conn, $get_sub_categories);
 ?>