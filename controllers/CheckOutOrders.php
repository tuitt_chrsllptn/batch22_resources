<?php 
	session_start();
	require"../controllers/connection.php";

	if (isset($_SESSION['cart'])) {
		$user_id = $_SESSION['user_id'];
		$total = 0;
		//first insert the user_id on the orders table
		$orders_qry = "INSERT INTO orders (user_id) VALUES ($user_id)";
		mysqli_query($conn, $orders_qry);

		//get the last inserted id, on the orders_qry
		$order_id = mysqli_insert_id($conn);
		//get the products in cart
		foreach ($_SESSION['cart'] as $product_id => $quantity) {
			$products_qry = "SELECT * FROM products WHERE product_id = $product_id";
			$run_products_qry = mysqli_query($conn, $products_qry);
			$product = mysqli_fetch_assoc($run_products_qry);
			//to get the total price to be inserted on the orders table
			$total += ($product['product_price'] * $quantity);

			//insert values on order_prodcuts table

			$order_products = "INSERT INTO order_products (order_id, product_id, quantity) VALUES ($order_id, $product_id, $quantity)";
			mysqli_query($conn, $order_products);

		}

		$update_orders = "UPDATE orders SET total = $total WHERE order_id = $order_id";
		mysqli_query($conn, $update_orders);

		unset($_SESSION['cart']);

		header('location: ../views/main.php');
	}

 ?>