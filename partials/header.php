<header>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<a class="navbar-brand" href="#"> <?php get_site_title(); ?> </a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../views/shop.php">Shop</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../views/mycart.php">Mycart 
						<span class="badge badge-light">
							<!-- if session cart has been initialized and has a number of elements inside, it will print the total sum of the values of session cart  -->						
							<?php if (isset($_SESSION['cart']) && count($_SESSION['cart'])) {
								echo array_sum($_SESSION['cart']);
							} ?>
						</span>
					</a>
				</li>
				<!-- admin panel -->
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Admin
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="../admin/admin.php">Admin panel</a>
						<a class="dropdown-item" href="#">Another action</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</li>
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
			<ul class="navbar-nav ml-auto">
				<?php if (isset($_SESSION['username'])): ?>
					
				<li class="nav-item">
					<a class="nav-link" href="../controllers/Logout.php">Logout</a>
				</li>
				<?php else: ?>
				<li class="nav-item">
					<a class="nav-link" href="../views/login.php">Login</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../views/register.php">Register</a>
				</li>
				<?php endif ?>
			</ul>
		</div>
	</nav>
</header>