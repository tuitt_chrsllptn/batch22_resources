<?php 
require"../controllers/connection.php";
// unset($_SESSION['cart']);
?>

<div class="container-fluid mt-4">
<div class="row justify-content-center">
<div class="col-md-10">

<?php if (isset($_SESSION['cart']) && count($_SESSION['cart'])): ?>
<table class="table">
	<thead>
		<tr>
			<th>Item</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Subtotal</th>

		</tr>
	</thead>
	<tbody>
		<!-- looping all the products inside the $_SESSION['cart'] --> 

		<?php $total = 0; /* initializing $total*/?>
		<?php foreach ($_SESSION['cart'] as $product_id => $quantity): ?>
			<?php
			$products_in_cart = "SELECT * FROM products WHERE product_id = $product_id";
			$run_products = mysqli_query($conn, $products_in_cart);
				//loop all the products
			$products = mysqli_fetch_assoc($run_products);
			extract($products); 
			/*to get the subtotal of the price and quantity of each product*/
			$subtotal = $product_price * $quantity;
			/*to get the overall total of the products*/
			$total += $subtotal;							
			?>
			<tr id=""> 
				<td><?php echo $product_title ?></td>
				<td>&#8369; <?php echo $product_price ?></td>
				<td>
					<form class="form-inline" method="POST" action="../controllers/AddToCart.php?id=<?= $product_id ?>">
						<!-- hidden input for upodate quantity/ flag to update the quantity on the mycart page-->
						<input type="hidden" name="updateCart" value="true"> 
						<input type="text" name="quantity" class="form-control mb-2 mr-sm-2" id="inlineFormquantity" placeholder="Quantity" value="<?php echo $quantity ?>">
						<button type="submit" class="btn btn-primary mb-2">Update quantity</button>
					</form>
				</td>
				<td>
					&#8369; <?php echo number_format($subtotal, 2) ?>
					<a href="../controllers/RemoveFromCart.php?id=<?= $product_id ?>" class="btn btn-danger">X</a>
				</td>
			</tr>
		<?php endforeach ?>
		<tr>
			<td>
				<strong>TOTAL</strong>: Php <?php echo number_format($total, 2) ?>
			</td>
			<td></td><td></td><td></td>
		</tr>
		<tr>
			<td>
				<a href="../controllers/EmptyCart.php" class="btn btn-danger">Empty Cart</a>
				<a href="../controllers/CheckOutOrders.php" class="btn btn-primary">Proceed to Checkout</a>
			</td>
			<td></td><td></td><td></td>
		</tr>
	</tbody>
</table>
<?php else: ?>
	<h2>No items in cart</h2>
<?php endif ?>
</div>
</div>
</div>