<?php
	require "../controllers/connection.php";

	$get_products = "SELECT * FROM products t1 JOIN sub_categories t2 ON t1.sub_category_id = t2.sub_category_id ORDER BY 1 DESC LIMIT 0, 4";

	$run_products = mysqli_query($conn, $get_products);

	$products = [];

	while($row = mysqli_fetch_assoc($run_products)) {
		$products[] = $row;
	}
?>

<div class="container home-products">
	<div class="row">
		<h2 class="col-md-12 mb-5 text-center"> <span class="border-bottom">Latest Products</span> </h2>

		<?php foreach($products as $key => $product) : ?>
			<div class="col-lg-3 col-md-4 mb-3">
				<div class="card">
					<img src="<?php echo $product['product_image'] ?>" alt="">
					<div class="card-body">
						<p class="card-category-title mb-0 text-secondary text-uppercase"> <?php echo $product['sub_category_title'] ?> </p>
						<h5 class="card-title mb-1"> <?php echo $product['product_title'] ?> </h5>
						<p class="card-text text-danger mb-1">&#8369; <?php echo number_format($product['product_price'], 2) ?></p>
						<a href="#" class="btn btn-ouline-primary btn-sm"> Add to Cart </a>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>