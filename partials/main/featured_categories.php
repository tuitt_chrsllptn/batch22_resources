<?php
	/*Connect to the DB*/
	require "../controllers/connection.php";

	/*SQL Statement*/
	$get_sub_categories = "SELECT * FROM sub_categories";

	/*Use the mysqli_query() function*/
	$run_sub_categories = mysqli_query($conn, $get_sub_categories);

	/*Initialize and empty array called  $sub_categories*/
	$sub_categories = [];

	/*Loop through all available rows*/
	while($row = mysqli_fetch_assoc($run_sub_categories)) {
		$sub_categories[] = $row;
	}
?>

<div class="container-fluid featured-categories">
	<div class="row py-3">
		<?php foreach($sub_categories as $key => $sub_category) : ?>
			<div class="col-lg-4 mt-3">
				<div class="card">
					<img src="<?php echo $sub_category['sub_category_image'] ?>" alt="">
					<div class="card-img-overlay d-flex flex-column justify-content-center align-items-center text-center">
						<h5 class="card-title text-white text-center text-uppercase bg-dark-custom px-3 py-2"> <?php echo $sub_category['sub_category_title'] ?> </h5>
						<a href="#" class="btn btn-secondary rounded-0"> Shop Now </a>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
