<?php
	/* Connect to DB */
	require "../controllers/connection.php";

	/* Write the SQL statement to fetch the services */
	$get_services = "SELECT * FROM services";

	/* Use mysqli_query function */
	$run_services = mysqli_query($conn, $get_services);

	/* Initialize an empty services array */
	$services = [];	

	/* Use while loop to fetch rows associatively */
	while($row = mysqli_fetch_assoc($run_services)) {
		$services[] = $row;
	}
?>

<div class="container services my-5">
	<div class="row">
		<?php foreach($services as $key => $service) : ?>
		<div class="col-md-4 mb-3">
			<div class="card text-center">
				<div class="card-body">
					<i class="<?php echo $service['service_icon'] ?>"></i>
					<h5 class="card-title"> <?php echo $service['service_name'] ?> </h5>
					<p class="card-text"><?php echo $service['service_description'] ?></p>
				</div>
			</div>
		</div>
		<?php endforeach ?>
	</div>
</div>












