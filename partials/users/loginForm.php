	<div class="container h-100">
		<div class="row">
			<div class="col-6 d-flex align-items-center justify-content-center">
				<div class="text-center">
					<h1 class="mb-5"> LOGIN HERE </h1>
					<form method="post" action="../controllers/loginController.php">
						<div class="form-group">
							<input type="text" class="form-control-lg border" id="username" name="username" placeholder="User name">
						</div>
						<div class="form-group">
							<input type="password" class="form-control-lg border" id="pwd" name="pwd" placeholder="Password">
						</div>
						<div class="form-group">
							<button id="submitBtn" class="btn btn-lg btn-primary form-control border">Login</button>
						</div>
						<div class="form-group">
							<span>Not a member? </span><a href="./register.php">Register here!</a>
						</div>
					</form>
				</div>
			</div>
			<div class="col-6" id="loginlayout">
				
			</div>
		</div>
	</div>