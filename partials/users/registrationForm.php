	<div class="container h-100">
		<div class="row">
			<div class="col-6" id="registerlayout">
				
			</div>
			<div class="col-6 d-flex align-items-center justify-content-center">
				<div class="text-center">
					<h1 class="mb-5"> REGISTER HERE </h1>
					<form method="post" action="../controllers/registrationController.php">
						<div class="form-group">
							<input type="text" class="form-control-lg border" id="username" name="username" placeholder="User name">
						</div>
						<div class="form-group">
							<input type="password" class="form-control-lg border" id="pwd" name="pwd" placeholder="Password">
						</div>
						<div class="form-group">
							<input type="password" class="form-control-lg border" id="confirmPwd" placeholder="Confirm Password">
						</div>
						<!-- <div class="form-group"> -->
							<button id="submitBtn" class="btn btn-lg btn-primary">Submit</button>
							<!-- </div> -->
							<div class="form-group">
								<span>Already a member? </span><a href="./login.php">Login here!</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

