<div class="container-fluid mt-5" id="admin-page">
	<div class="row">
		<!-- Buttons -->
		<div class="col-3">
			<div class="nav flex-column nav-pills border border-secondary rounded" id="v-pills-tab" role="tablist" aria-orientation="vertical">

				<!-- Product-list -->
				<a class="nav-link text-dark text-left m-2 " id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><h4>Product List</h4></a>

				<!-- Add Product -->
				<a class="nav-link text-dark text-left m-2 " id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><h4>Add Product</h4></a>

				<!-- Orders -->
				<a class="nav-link text-dark text-left m-2 " id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><h4>Orders</h4></a>

				<!-- Settings  -->
				<a class="nav-link text-dark text-left m-2 " id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><h4>Settings</h4></a>

			</div>
		</div>

		<!-- Content -->
		<div class="col-9">
			<div class="tab-content" id="v-pills-tabContent">

				<!-- Product-list -->
				<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
					<?php require"product_list.php" ?>
				</div>

				<!-- add Product -->
				<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
					<?php require"product_form.php"; ?>
				</div>


				<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
				<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
			</div>
		</div>
	</div>
	
</div>