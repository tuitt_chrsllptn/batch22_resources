<?php 
require"../controllers/AdminController.php";
?>

<div class="container-fluid">
<div class="py-5 text-center">
<h2 class="display-4">List of Products</h2>

<!-- //check if $_SESSION['danger'] is initialized -->
<?php if (isset($_SESSION['danger'])): ?>

<div class="alert alert-danger alert-dismissible fade show" role="alert">
<strong>Holy guacamole!</strong> <?php echo $_SESSION['danger'] ?>
<button type="button" class="close" data-dismiss="alert">
<span>&times;</span>
</button>
</div>

<?php unset($_SESSION['danger']); ?>
<?php endif ?>

</div>

<div class="row d-flex justify-content-center">
<div class="col-md-10 mb-5">
<table class="table table-bordered text-center">
<thead>
<tr>
<th scope="col">Product title</th>
<th scope="col">Product price</th>
<th scope="col">Product description</th>
<th scope="col">Product image</th>
<th scope="col">Category</th>
<th scope="col">Product category</th>
<th colspan="2">Action</th>
</tr>
</thead>
<tbody>

<?php foreach ($run_products as $key => $product):
extract($product);
?>
<tr>
<td><?php echo $product_title ?></td>
<td><?php echo $product_price ?></td>
<td><?php echo $product_description ?></td>
<td><img src="../assets/images/products/<?php echo $product_image ?>" class="img-fluid"></td>
<td>
<?php echo $sub_category_title ?>
</td>
<td><?php echo $category_title ?></td>
<td>
<!-- Button trigger update modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#updateModal<?php echo $product_id?>">
Edit
</button>

</td>
<td>
<!-- Button delete modal -->
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal<?php echo $product_id ?>">
Delete
</button>
</td>
</tr>

<!-- MODALS -->

<!-- Update modal -->
<div class="modal fade" id="updateModal<?php echo $product_id?>" tabindex="-1" role="dialog" aria-labelledby="updateModallabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="updateModallabel">Update Item</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	<span aria-hidden="true">&times;</span>
</button>
</div>
<!-- form -->
<form method="POST" action="../controllers/UpdateProduct.php?id=<?= $product_id ?>" enctype="multipart/form-data">
<div class="modal-body">
	<input type="text" name="id"> value="<?php echo $product_id ?>" hidden>
	<div class="form-group">
		<label for="product_title">Product Title</label>
		<input type="text" class="form-control" id="product_title" name="product_title" value="<?php echo $product_title ?>">
	</div>
	<div class="form-group">
		<label for="product_cat">
			Product Category
		</label>
		<select name="product_cat" id="product_cat" class="form-control custom-select">
			<option value="<?php echo $sub_category_id ?>"selected>
				<?php echo $sub_category_title ?>
			</option>

			<?php foreach ($run_sub_categories as $key => $sub_categories):?>
				<?php if ($sub_category_id === $sub_categories['sub_category_id']): ?> -->
				<option value="<?php echo $sub_categories['sub_category_id'] ?>" selected><?php echo $sub_categories['sub_category_title']; ?>
				</option>
				<?php else: ?>
					<option value="<?php echo $sub_categories['sub_category_id'] ?>"><?php echo $sub_categories['sub_category_title'] ?>
				</option>
			<?php endif; ?>
		<?php endforeach ?>
	</select>
</div>
<div class="form-group">
	<label for="cat">Category</label>
	<select name="cat" id="cat" class="form-control custom-select">
		<option selected>Select a Category</option>
		<?php foreach ($run_categories as $key => $categories):?>
			<?php if ($category_id === $categories['category_id']): ?>
				<option value="<?php echo $categories['category_id'] ?>" selected><?php echo $categories['category_title']; ?>
			</option>
			<?php else: ?>
				<option value="<?php echo $categories['category_id'] ?>"><?php echo $categories['category_title'] ?>
			</option>
		<?php endif; ?>
	<?php endforeach ?>
</select>
</div>
<div class="form-group">
<label>Product Image</label>
<input type="file" name="product_image" class="form-control" value="">
</div>
<div class="form-group">
<label for="product_price">Product Price</label>
<input type="number" class="form-control" id="product_price" name="product_price" value="<?php echo $product_price ?>">
</div>
<div class="form-group">
<label for="product_description">Product Description</label>
<textarea class="form-control" id="product_description" name="product_description"><?php echo $product_description ?></textarea>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-primary">Save changes</button>
</div>
</form>  <!-- end of form -->
</div>
</div>
</div>
<!-- end of update modal -->

<!-- Delete Modal -->
<div class="modal fade" id="deleteModal<?php echo $product_id ?>" tabindex="-1" role="dialog" aria-labelledby="deleteModallabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="deleteModallabel">Delete item</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<h4>Are you sure you want to delete this product?</h4>
<small>You can not undo this.</small>
</div>
<div class="modal-footer">
<form action="../controllers/DeleteProduct.php?id=<?= $product_id ?>" method="POST"> <!-- form -->
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-danger">Delete</button>
</form> <!-- end of form -->
</div>
</div>
</div>
</div>
<!-- end of Delete Modal -->


<?php endforeach; ?>
</tbody>
</table>
</div>
</div>
</div>