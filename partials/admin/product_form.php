<?php require"../controllers/AdminController.php"; ?>

<div class="container-fluid">
	<div class="py-5 text-center">
	  <h2 class="display-4">Insert Products</h2>
	  <p class="lead">You can insert products here</p>

			<div class="alert alert-success alert-dismissible fade show" role="alert">
			  <strong>Holy guacamole!</strong> 
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		
	</div>

	<div class="row d-flex justify-content-center">
		<div class="col-md-10 mb-5">
			<form method="POST" action="../controllers/AddItem.php" enctype="multipart/form-data">
			  <div class="form-group">
			    <label for="product_title">Product Title</label>
			    <input type="text" class="form-control" id="product_title" name="product_title" placeholder="e.g. Plain White T-Shirt">
			  </div>
			  <div class="form-group">
			  	<label for="product_cat">Product Category</label>
			  	<select name="product_cat" id="product_cat" class="form-control custom-select">
					  <option selected>Select a Product Category</option>
					  <?php foreach ($run_sub_categories as $key => $sub_categories): extract($sub_categories); ?>
					  <option value="<?php echo $sub_category_id ?>"><?php echo $sub_category_title; ?></option>
					  <?php endforeach ?>
					</select>
			  </div>
			  <div class="form-group">
			  	<label for="cat">Category</label>
			  	<select name="cat" id="cat" class="form-control custom-select">
					  <option selected>Select a Category</option>
					  <?php foreach ($run_categories as $key => $category): extract($category);?>
					  	<option value="<?php echo $category_id ?>"><?php echo $category_title; ?></option>
					  <?php endforeach ?>
				</select>
			  </div>
			  <div class="form-group">
			  	<label>Product Image</label>
			  	<input type="file" name="product_image" class="form-control">
			  </div>
			  <div class="form-group">
			    <label for="product_price">Product Price</label>
			    <input type="number" class="form-control" id="product_price" name="product_price" placeholder="e.g. 420.00">
			  </div>
			  <div class="form-group">
			    <label for="product_description">Product Description</label>
			    <textarea class="form-control" id="product_description" name="product_description" placeholder="A cheeseburger is more than just a sandwich, it is a wish fulfilled..."></textarea>
			  </div>
			  <button type="submit" name="submit" class="btn btn-outline-primary">Add Product</button>
			</form>
		</div>
	</div>
</div>