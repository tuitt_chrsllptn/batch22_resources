 <div class="container">
 	<div class="row">
 		<!-- Side bar -->
 		<div class="col-md-3 d-lg-block d-none shop-sidebar">
 			<?php require"sidebar.php"; ?>
 		</div>
 		
		<!-- products -->
 		<div class="col-lg-9 col-md-12">
 			<div class="col-md-12 border mb-3">
 				<h2>Shop</h2>
 				<p class="lead">
 					The palatable sensation we lovingly refer to as The Cheeseburger has a distinguished and illustrious history. It was born from humble roots, only to rise to well-seasoned greatness.
 				</p>
 			</div>
 			<?php require"products.php"; ?>
 		</div>
 	</div>
 </div>