<?php 
require "../controllers/connection.php";

$get_product_categories = "SELECT * FROM sub_categories";

$run_prod_categories = mysqli_query($conn, $get_product_categories);

$product_categories = [];

while($row = mysqli_fetch_assoc($run_prod_categories)) {
	$product_categories[] = $row;
}

$get_categories = "SELECT * FROM categories";

$categories = mysqli_query($conn, $get_categories);

?>

<div class="sticky-top">

	<div class="accordion mb-3" id="product-categories">
		<!-- Product Categories -->
		<div class="card">
			<div class="card-header bg-secondary" id="product-cat-heading">
				<h2 class="mb-0">
					<button class="btn btn-link d-block text-left text-white" type="button" data-toggle="collapse" data-target="#product-categories-collapse" aria-expanded="true" aria-controls="collapseOne">
						Product Categories
					</button>
				</h2>
			</div>

			<div id="product-categories-collapse" class="collapse show" aria-labelledby="product-cat-heading" data-parent="#product-categories">
				<div class="card-body">
					<ul class="list-group list-group-flush">
						<?php foreach ($product_categories as $key => $product_category): 
							extract($product_category); ?>
							<li class="list-group-item">
								<?php echo $sub_category_title ?>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>

		<!-- Categories -->
		<div class="card">
			<div class="card-header bg-secondary" id="categories">
				<h2 class="mb-0">
					<button class="btn btn-link d-block text-left text-white collapsed" type="button" data-toggle="collapse" data-target="#categories-collapse" aria-expanded="false" aria-controls="collapseTwo">
						Categories
					</button>
				</h2>
			</div>
			<div id="categories-collapse" class="collapse" aria-labelledby="categories" data-parent="#product-categories">
				<div class="card-body">
					<ul class="list-group list-group-flush">
						<?php foreach ($categories as $key => $category): ?>
							<li class="list-group-item">
								<?php echo $category['category_title'] ?>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
</div>