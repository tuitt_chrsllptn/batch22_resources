<?php 
require "../controllers/connection.php";

$get_products = "SELECT * FROM products t1 JOIN sub_categories t2 WHERE t1.sub_category_id = t2.sub_category_id ORDER BY 1 DESC";

$products_qry = mysqli_query($conn, $get_products);
?>

<div class="row shop-products">
	<?php foreach ($products_qry as $key => $product): ?>
		<div class="col-lg-4 col-md-4 mb-3">
			<div class="card">
				<img src="<?php echo $product['product_image'] ?>" alt="">
				<div class="card-body">
					<h5 class="card-title"><?php echo $product['product_title'] ?></h5>
					<p class="card-text"><?php echo $product['sub_category_title'] ?></p>
					<p class="card-text">&#8369; <?php echo number_format($product['product_price'], 2);?></p>
					<form method="POST" action="../controllers/AddToCart.php?id=<?= $product['product_id']?>">
				    	<div class="form-group row">
				    		<label class="col-sm-5 col-form-label col-form-label-sm">Quantity:</label>
				    		<input type="number" name="quantity" class="form-control col-sm-7">
				    	</div>
				    	<div class="form-group row justify-content-center">
						    <button type="submit" class="form-control btn btn-outline-primary btn-sm">Add to Cart</button>
				    	</div>
				    </form>
				</div>
			</div>
		</div>
	<?php endforeach; ?>

</div>