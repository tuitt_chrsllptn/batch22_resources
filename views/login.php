<?php 
session_start();

//if the user tries to access the login page even if he/she is authenticated
if (isset($_SESSION['username'])) {
	header('location: ../views/main.php');
} else{
	function get_content(){
		require"../partials/users/loginForm.php";
	}

	require"../partials/template.php";
}


